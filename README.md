# Changelog Generator

This program generates changelog of your program from commits in a markdown format. This is convenient to build a changelog for your releases.

## Install
```
npm install
```

## Usage
```
node changelog.js <owned/project> <tag> <previous-version>
```

## Example

If I want to generate a changelog for [`DSpot-3.1.0`](), _i.e._ from the version `3.0.0` to `3.1.0` (the next version), I would type:
```
../changelog/changelog-generator STAMP-project/dspot dspot 3.0.0
```

from the folder containing the `.git` folder.

It should give the following output:

```txt

# New features

* Support deprecated assertions class of junit3. (PR: #926)


# Bug Fixes

* Run example from jar FIX #919. (PR: #934)
* Add a default value for --pit-filter-classes-to-keep command line option. (PR: #929)
* Fix is collection object log. (PR: #927)
* Default value for test methods command line option. (PR: #925)
* Use now imports and not qualified name in DSpotUtilsTest. (PR: #924)


# Documentation

* Update string amplification with corner cases Fix #920.


# Tests

* Relax expected properties. (PR: #935)


# Refactoring

* Organize packages to reflect the algorithm. (PR: #933)
* Main, DSpot and Amplification. (PR: #930)

# Authors
| Name    | Nb Commit |
|---------|-----------|
| Benjamin DANGLOT | 8 |
| andrewbwogi | 2 |
```

## Credits

The original authors is [Thomas Durieux](https://durieux.me), and was developped for generating dedicated changelogs for [Spoon](https://github.com/INRIA/spoon/tree/8604b4bb86fd080915edd4cb9dc44d0866f3d59b/doc/_release/changelog_generator).

I did some modifications to support any git project.

## Build from sources

Clone, build and package:
```
git clone https://gitlab.com/danglotb/changelog-generator.git
cd changelog-generator
npm install
pkg . --target node10 --output bin/changelog-generator
```

You should obtain a changelog-generator executable. Put the executable in your PATH to execute it from anywhere.

```
echo 'export PATH=${PATH}:'$(pwd)'/bin/' >> ~/.bashrc
echo 'export PATH=${PATH}:'$(pwd)'/bin/' >> ~/.zshrc
```



Command-line to build binaries for most common platforms:
```
pkg . --target node10-win,node10-linux,node10-macos
```
